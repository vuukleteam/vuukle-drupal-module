/**
* @file
* Attaches behaviors for the Vuukle module.
*/          
(function ($) {

    "use strict";   

    $(".content .node__content").before("<div class='vuuklehtmlbefore'>Vuukle Comment System Initialising.....</div>");
    $(".content .node__content").after("<div class='vuuklehtmlafter'>Vuukle Comment System Initialising.....</div>");

    localStorage.vuukle = "0";
     
    /**
    *
    * @type {Drupal~behavior}
    */
    Drupal.behaviors.vuukleSettings = {

        attach: function () {

            if(localStorage.vuukle == "0") {

                $(".field--type-comment").remove();

                var UNIQUE_ARTICLE_ID = drupalSettings.vuukle.node_id;
                var VUUKLE_API_KEY = drupalSettings.vuukle.vuukle_api_key;
                var node_title = drupalSettings.vuukle.node_title;
                node_title = node_title.replace(" ", "+");

                var author = drupalSettings.vuukle.author;

                var userid = drupalSettings.vuukle.userid;
                var username = drupalSettings.vuukle.username;
                var useremail = drupalSettings.vuukle.useremail;

                var content_type = drupalSettings.vuukle.content_type;
                var vuukle_content_type = drupalSettings.vuukle.vuukle_content_type;
                var ARTICLE_AUTHORS = {};
                var tags = '';

                var base_url = window.location.origin;
                var protocol = document.location.protocol.replace(':', '');

                var sso = drupalSettings.vuukle.sso;
                var language = drupalSettings.vuukle.language;
                var languageEnabledByDefault = drupalSettings.vuukle.languageEnabledByDefault;
                var maxChars = drupalSettings.vuukle.maxChars;
                var Param4 = drupalSettings.vuukle.Param4;
                Param4 = Param4.replace("#", "");
                var maxChars = drupalSettings.vuukle.maxChars;
                var emote = drupalSettings.vuukle.emote;
                var happy_text = drupalSettings.vuukle.happy_text;
                var happy_url = drupalSettings.vuukle.happy_url;
                var indifferent_text = drupalSettings.vuukle.indifferent_text;
                var indifferent_url = drupalSettings.vuukle.indifferent_url;
                var amused_text = drupalSettings.vuukle.amused_text;
                var amused_url = drupalSettings.vuukle.amused_url;
                var excited_text = drupalSettings.vuukle.excited_text;
                var excited_url = drupalSettings.vuukle.excited_url;
                var angry_text = drupalSettings.vuukle.angry_text;
                var angry_url = drupalSettings.vuukle.angry_url;
                var sad_text = drupalSettings.vuukle.sad_text;
                var sad_url = drupalSettings.vuukle.sad_url;
                var share = drupalSettings.vuukle.share;
                var share_position = drupalSettings.vuukle.share_position;
                var share_type = drupalSettings.vuukle.share_type;
                var share_vertical_styles = drupalSettings.vuukle.share_vertical_styles;
                var newsletter = drupalSettings.vuukle.newsletter;
                var newsletter_position = drupalSettings.vuukle.newsletter_position;
                var div_id = drupalSettings.vuukle.div_id;
                var div_id_powerbar = drupalSettings.vuukle.div_id_powerbar;
                var div_id_emotes = drupalSettings.vuukle.div_id_emotes;
                var priority = drupalSettings.vuukle.priority;
                var newsletter_priority = drupalSettings.vuukle.newsletter_priority;
                var sorting = drupalSettings.vuukle.sorting;
                var recommendationsWideImages = drupalSettings.vuukle.recommendationsWideImages;
                var globalRecommendations = drupalSettings.vuukle.globalRecommendations;
                var darkMode = drupalSettings.vuukle.darkMode;
                var hideRecommendedArticles = drupalSettings.vuukle.hideRecommendedArticles;
                var hideCommentInputBox = drupalSettings.vuukle.hideCommentInputBox;
                var commentingClosed = drupalSettings.vuukle.commentingClosed;
                var countToLoad = drupalSettings.vuukle.countToLoad;
                var toxicityLimit = drupalSettings.vuukle.toxicityLimit;
                var enabledComments = drupalSettings.vuukle.enabledComments;
                var size_emote = drupalSettings.vuukle.size_emote;

                var hostName = window.location.host;
                
                var before_response = "";
                var after_response = "";

                // alert("sso => " + sso + ", language => " + language + ", languageEnabledByDefault => " + languageEnabledByDefault + ", maxChars => " + maxChars + ", Param4 => " +  Param4 + ", maxChars => " + maxChars + ", emote => " + emote + ", happy_text => " + happy_text + ", happy_url => " + happy_url + ", indifferent_text => " + indifferent_text + ", indifferent_url => " + indifferent_url + ", amused_text => " + amused_text + ", amused_url => " + amused_url + ", excited_text => " + excited_text + ", excited_url => " + excited_url + ", angry_text => " + angry_text + ",  angry_url => " + angry_url + ", sad_text => " + sad_text + ", sad_url => " + sad_url + ", share => " + share + ", share_position => " + share_position + ", share_type  > " + share_type + ", share_vertical_styles => " + share_vertical_styles + ", newsletter => " +  newsletter + ", newsletter_position => " + newsletter_position + ", div_id => " + div_id + ", div_id_powerbar => " + div_id_powerbar + ", div_id_emotes => " + div_id_emotes + ", priority => " +  priority + ", newsletter_priority => " + newsletter_priority + ", sorting => " + sorting + ", recommendationsWideImages => " + recommendationsWideImages + ", globalRecommendations => " +  globalRecommendations + ", darkMode => " + darkMode + ", hideRecommendedArticles => " +  hideRecommendedArticles + ", hideCommentInputBox => " + hideCommentInputBox + ", commentingClosed => " + commentingClosed + ", countToLoad => " + countToLoad + ", toxicityLimit => " +  toxicityLimit + ", enabledComments => " + enabledComments );



                if (userid > '0' && sso == '1') { 
                    var script = "s.addEventListener('load', function() { vuukleAuthUser('" + username + "', '" + useremail + "'); });";

                } else {
                    var script = "";
                }

                if(share == "1") { var sshare = "true"; } else { var sshare = "false"; }

                $.each(share_position, function() {
                    var key = Object.keys(this)[0];
                    var spvalue = this[key];

                    if(share_type == "vertical") { var share_type_class = "vuukle-powerbar-vertical"; } else { var share_type_class = "vuukle-powerbar"; }

                    if(spvalue == "2" && share == "1") {       
                        before_response += "<div id='vuukle-powerbar' class='" + share_type_class + " powerbarBoxDiv' style=' '></div>";
                    }
                });            

                $.each(share_position, function() {
                    var key = Object.keys(this)[0];
                    var spvalue = this[key];

                    if(share_type == "vertical") { 
                        var share_type_class = "vuukle-powerbar-vertical"; 
                    } else { 
                        var share_type_class = "vuukle-powerbar"; 
                    }

                    if(spvalue == "1" && share == "1") {       
                        after_response += "<div id='vuukle-powerbar' class='" + share_type_class + " powerbarBoxDiv' style=' '></div>";
                    }
                });

                
                after_response += "<script>";
                    after_response += "var VUUKLE_EMOTE_SIZE = '';";
                    after_response += "VUUKLE_EMOTE_IFRAME = ''";
                after_response += "</script>";
                after_response += "<script>";
                    after_response += "var EMOTE_TEXT = ['HAPPY', 'INDIFFERENT', 'AMUSED', 'EXCITED', 'ANGRY', 'SAD'];";
                after_response += "</script>";

                after_response += "<div id='vuukle-emote' class='emotesBoxDiv'></div>";
                after_response += "<div id='respond'></div>";
                after_response += "<div id='vuukle-comments' class='commentBoxDiv'></div>";

                after_response += "<script>";
                    after_response += "var VUUKLE_CONFIG = {";
                        after_response += "apiKey: '" + VUUKLE_API_KEY + "'";
                        after_response += ", articleId: '" + UNIQUE_ARTICLE_ID + "'";
                        after_response += ", tags: ''";
                        after_response += ", author: '" + author + "'";
                        after_response += ", wordpressSync: false";
                        after_response += ", eventHandler: function (e) {";
                        after_response += "console.log(e);";
                        after_response += "}";
                        after_response += ", recommendationsWideImages: " + recommendationsWideImages + "";
                        after_response += ", recommendationsProtocol: '" + protocol + "'";
                        after_response += ", globalRecommendations: " + globalRecommendations + "";
                        after_response += ", darkMode: " + darkMode + "";
                        after_response += ", color: '" + Param4 + "'";
                        after_response += ", comments: {";
                            after_response += "hideRecommendedArticles: " + hideRecommendedArticles + "";
                            after_response += ", hideCommentInputBox: " + hideCommentInputBox + "";
                            after_response += ", enabled: " + enabledComments + "";
                            after_response += ", commentingClosed: " + commentingClosed + "";
                            after_response += ", maxChars: '" + maxChars + "'";
                            after_response += ", countToLoad: '" + countToLoad + "'";
                            after_response += ", toxicityLimit: '" + toxicityLimit + "'";
                            after_response += ", sorting: '" + sorting + "'";
                            after_response += ", transliteration: {";
                                after_response += "language: '" + language + "'";
                                after_response += ", enabledByDefault: " + languageEnabledByDefault + "";
                            after_response += ", }";
                            after_response += ", customText: {}";
                        after_response += ", }";
                        after_response += ", emotes: {";
                            after_response += "enabled: " + emote + "";
                            after_response += ", hideRecommendedArticles: " + hideRecommendedArticles + "";
                            after_response += ", size: '" + size_emote + "'";
                            after_response += ", firstImg: '" + happy_url + "'";
                            after_response += ", firstName: '" + happy_text + "'";
                            after_response += ", secondImg: '" + indifferent_url + "'";
                            after_response += ", secondName: '" + indifferent_text + "'";
                            after_response += ", thirdImg: '" + amused_url + "'";
                            after_response += ", thirdName: '" + amused_text + "'";
                            after_response += ", fourthImg: '" + excited_url + "'";
                            after_response += ", fourthName: '" + excited_text + "'";
                            after_response += ", fifthImg: '" + angry_url + "'";
                            after_response += ", fifthName: '" + angry_text + "'";
                            after_response += ", sixthImg: '" + sad_url + "'";
                            after_response += ", sixthName: '" + sad_text + "'";
                            after_response += ", disable: []";
                            after_response += ", customText: {}";
                        after_response += ", }";
                        after_response += ", powerbar: {";
                            after_response += "enabled: " + sshare + "";
                            after_response += ", defaultEmote: 1";
                            after_response += ", customUrls: {";
                                after_response += "facebook: ''";
                                after_response += ", google: ''";
                                after_response += ", twitter: ''";
                                after_response += ", linkedin: ''";
                            after_response += ", }";
                            after_response += ", customText: {}";
                        after_response += ", }";
                    after_response += ", };";
                    after_response += "(function () {";
                        after_response += "var d = document";
                            after_response += ", s = d.createElement('script');";
                        after_response += "s.async = true;";
                        after_response += "s.src = 'https://cdn.vuukle.com/platform.js';";
                        after_response += "(d.head || d.body)";
                        after_response += ".appendChild(s);";
                        // alert(userid + "<-=-=-=->" + sso + "<-=-=-=->" + username + "<-=-=-=->" + useremail)
                        if (userid > '0' && sso == '1') { 
                            after_response += "s.addEventListener('load', function() {";
                                after_response += "vuukleAuthUser('" + username + "', '" + useremail + "');";
                            after_response += "});";
                        } else {
                            after_response += "";
                        }
                        //after_response += "" + script + "";
                    after_response += "})();";
                after_response += "</script>";
                
                // alert(before_response + "<-=-=-=->" + after_response)


                $.each(vuukle_content_type, function() {
                    var key = Object.keys(this)[0];
                    var spvalue = this[key];
                    
                    if(spvalue == content_type || spvalue == "a") {       
                        $(".vuuklehtmlbefore").html(before_response);                      
                        $(".vuuklehtmlafter").html(after_response);
                    }
                });  

                $.each(vuukle_content_type, function() {
                    var key = Object.keys(this)[0];
                    var spvalue = this[key];
                    
                    if(spvalue == content_type || spvalue == "a") {      
                        $(".vuuklehtmlbefore").html(before_response);                      
                        $(".vuuklehtmlafter").html(after_response);
                    }
                });              

                localStorage.vuukle = "1";
            }
                
        }
    };

})(jQuery);