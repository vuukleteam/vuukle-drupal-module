<?php

namespace Drupal\vuukle\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class VuukleConfigForm.
 *
 * @package Drupal\vuukle\Form
 */
class VuukleConfigForm extends ConfigFormBase {

  public $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
    $this->config = $this->config('vuukleconfig.setting');
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vuukleconfig.setting',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vuukle_config_form';
  }
  
  
    public function vuukleResponse() {
    echo 'This is response!';
  }




  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $node_type = node_type_get_names();
    
    if($this->config->get('vuukle_content_type'))
    {
      $defaultcontent_type = $this->config->get('vuukle_content_type');
    }
    else
    {
      $defaultcontent_type = array('article');
    }
   
    $form['vuukle_content_type'] = [
      '#type' => 'checkboxes',
      '#options' => $node_type,
      '#default_value' => array('article' => 'article', 'page' => 'page'),
      '#title' => $this->t('Select content type for which you want vuukle enable'),
      '#attributes' => array('readonly' => 'readonly'),
    ];
             
   if($this->config->get('vuukle_api_key'))
   {
     $defaultvuukle_api_key = $this->config->get('vuukle_api_key');
   }
   else
   {
        global $base_url;
        $url = $base_url;;
        $email = \Drupal::config('system.site')->get('mail');
      
        $curl = curl_init();
        curl_setopt_array($curl, 
            array(
                CURLOPT_URL => "http://vuukle.com/api.asmx/quickRegister",
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5,
                CURLOPT_HTTP_VERSION => 1.0,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "url=" . $url . "&email=" . $email . "&timeout=5&redirection=5&httpversion=1.0&blocking=true",
                CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        // $response = curl_exec($curl);
        $response = explode(',', str_replace(array('{','}'), '', curl_exec($curl)));
        $err = curl_error($curl);
        curl_close($curl);

        $response1 = explode(':', str_replace(array('{','}'), '', $response['0']));
        $response2 = explode(':', str_replace(array('{','}'), '', $response['1']));

        $biz_id = trim($response1['2']);
        $born_now = $response2['1'];
        
        $defaultvuukle_api_key = str_replace('"', '', $biz_id);
         
   }

    $form['vuukle_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Vuukle api key'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#attributes' => array('readonly' => 'readonly'),
      '#default_value' => $defaultvuukle_api_key,
    ];
   
   
    if($this->config->get('sso'))
    {
      $defaultsso = $this->config->get('sso');
    }
    else
    {
      $defaultsso = 1;
    }

    $form['sso']= [
        '#type' => 'radios',
        '#title' => t('SSO'),
        '#options' => array(0 => 'Off', 1 => 'On'),
        '#required'=>TRUE,
        '#default_value' =>$defaultsso,
    ];

$options = array();
$options["en"] = "ENGLISH";
$options["am"] = "AMHARIC";
$options["ar"] = "ARABIC";
$options["bn"] = "BENGALI";
$options["zh"] = "CHINESE";
$options["el"] = "GREEK";
$options["gu"] = "GUJARATI";
$options["hi"] = "HINDI";
$options["kn"] = "KANNADA";
$options["ml"] = "MALAYALAM";
$options["mr"] = "MARATHI";
$options["ne"] = "NEPALI";
$options["or"] = "ORIYA";
$options["fa"] = "PERSIAN";
$options["pa"] = "PUNJABI";
$options["ru"] = "RUSSIAN";
$options["sa"] = "SANSKRIT";
$options["si"] = "SINHALESE";
$options["sr"] = "SERBIAN";
$options["ta"] = "TAMIL";
$options["te"] = "TELUGU";
$options["ti"] = "TIGRINYA";
$options["ur"] = "URDU";


    if($this->config->get('language'))
    {
      $defaultlanguage= $this->config->get('language');
    }
    else
    {
      $defaultlanguage= 'en';
    }

$form['language'] =[
          '#type' => 'select',
          '#title' => t('Language(Google Transliterate)'),
          '#options' => $options,
          '#required'=>TRUE,
          '#default_value' => $defaultlanguage,
       ];

    if($this->config->get('languageEnabledByDefault'))
    {
      $defaultlanguageEnabledByDefault= $this->config->get('languageEnabledByDefault');
    }
    else
    {
      $defaultlanguageEnabledByDefault= 'false';
    }

  
  $form['languageEnabledByDefault']= [
  '#type' => 'radios',
  '#title' => t('Default Language '),
  '#options' => array('false' => 'No', 'true' => 'Yes'),
  '#required'=>TRUE,
  '#default_value' =>$defaultlanguageEnabledByDefault,
];



    if($this->config->get('maxChars'))
    {
      $defaultmaxChars= $this->config->get('maxChars');
    }
    else
    {
      $defaultmaxChars= '3000';
    }

    $form['maxChars'] = [
      '#type' => 'number',
      '#title' => $this->t('Comment Character Limit'),
      '#required' => TRUE,
      '#min' => 1,
      '#default_value' => $defaultmaxChars,
    ];


        if($this->config->get('Param4'))
    {
      $defaultParam4= $this->config->get('Param4');
    }
    else
    {
      $defaultParam4= '#108ee9';
    }
    
      $form['Param4'] = [
      '#type' => 'color',
      '#title' => $this->t('Select customize color '),
      '#required' => TRUE,
      '#default_value' => $defaultParam4,
    ];


    if($this->config->get('emote'))
    {
      $defaultemote= $this->config->get('emote');
    }
    else
    {
      $defaultemote= 'true';
    }
    
   
      $form['emote']= [
  '#type' => 'radios',
  '#title' => t('Show Emote at the end of each post'),
  '#options' => array('false' => 'No', 'true' => 'Yes'),
  '#required'=>TRUE,
  '#default_value' =>$defaultemote,
];
      

    if($this->config->get('happy_text'))
    {
      $defaulthappy_text = $this->config->get('happy_text');
    }
    else
    {
      $defaulthappy_text= 'HAPPY';
    }
    
    
      $form['happy_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Emote first name '),
      '#default_value' =>$defaulthappy_text,
    ];
    
      $form['happy_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Emote first url image '),
      '#default_value' => $this->config->get('happy_url'),
    ];
    
    

    if($this->config->get('indifferent_text'))
    {
      $defaultindifferent_text = $this->config->get('indifferent_text');
    }
    else
    {
      $defaultindifferent_text = 'INDIFFERENT';
    }
    


      $form['indifferent_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Emote second name '),
      '#default_value' => $defaultindifferent_text ,
    ];
    
    
    
      $form['indifferent_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Emote second url image'),
      '#default_value' => $this->config->get('indifferent_url'),
    ];
    
     if($this->config->get('amused_text'))
    {
      $defaultamused_text = $this->config->get('amused_text');
    }
    else
    {
      $defaultamused_text= 'AMUSED';
    }
    
      $form['amused_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Emote third name  '),
      '#default_value' => $defaultamused_text,
    ];
    
    
      $form['amused_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Emote third url image '),
      '#default_value' => $this->config->get('amused_url'),
    ];
    
    
    if($this->config->get('excited_text'))
    {
      $defaultexcited_text = $this->config->get('excited_text');
    }
    else
    {
      $defaultexcited_text= 'EXCITED';
    }
    
    
       $form['excited_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Emote fourth name '),
      '#default_value' => $defaultexcited_text,
    ];
    
    
      $form['excited_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Emote fourth url image'),
      '#default_value' => $this->config->get('excited_url'),
    ];
    
    if($this->config->get('angry_text'))
    {
      $defaultangry_text = $this->config->get('angry_text');
    }
    else
    {
      $defaultangry_text = 'ANGRY';
    }
     
        
      $form['angry_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Emote fifth name '),
      '#default_value' => $defaultangry_text,
    ];
    
    
      $form['angry_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Emote fifth url image '),
      '#default_value' => $this->config->get('angry_url'),
    ];
    
    
    if($this->config->get('sad_text'))
    {
      $defaultsad_text = $this->config->get('sad_text');
    }
    else
    {
      $defaultsad_text = 'SAD';
    }
    
    
      $form['sad_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Emote sixth name '),
      '#default_value' => $defaultsad_text,
    ];
    
    
      $form['sad_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Emote sixth url image '),
      '#default_value' => $this->config->get('sad_url'),
    ];
    
     if($this->config->get('share'))
    {
      $defaultshare = $this->config->get('share');
    }
    else
    {
      $defaultshare = '1';
    }   
    
   $form['share']= [
  '#type' => 'radios',
  '#title' => t('Show Share Bar '),
  '#options' => array(0 => 'Off', 1 => 'On'),
  '#required'=>TRUE,
  '#default_value' =>$defaultshare,
];



     if($this->config->get('share_position'))
    {
      $defaultshare_position = $this->config->get('share_position');
    }
    else
    {
      $defaultshare_position = array('2');
    }  
    
    
    $form['share_position'] = [
      '#type' => 'checkboxes',
      '#options' => array(1 => 'After Content Post', 2 => 'Before Content Post'),
      '#default_value' => $defaultshare_position,
      '#title' => $this->t('Share Bar Position'),
    ];



     if($this->config->get('share_type'))
    {
      $defaultshare_type = $this->config->get('share_type');
    }
    else
    {
      $defaultshare_type = 'horizontal';
    }  
    
        
    
  $form['share_type']= [
  '#type' => 'select',
  '#title' => t('Share Bar Type '),
  '#options' => array('horizontal' => 'Horizontal', 'vertical' => 'Vertical'),
  '#required'=>TRUE,
  '#default_value' =>$defaultshare_type,
];

      $form['share_vertical_styles'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Share Bar Styles (only for vertical type)'),
      '#default_value' => $this->config->get('share_vertical_styles'),
    ];
    
    
   if($this->config->get('newsletter'))
    {
      $defaultnewsletter = $this->config->get('newsletter');
    }
    else
    {
      $defaultnewsletter = '0';
    }  
    
    
          $form['newsletter']= [
  '#type' => 'radios',
  '#title' => t('Show Newsletter Bar'),
  '#options' => array(0 => 'Off', 1 => 'On'),
  '#required'=>TRUE,
  '#default_value' =>$defaultnewsletter,
];


    
    if($this->config->get('newsletter_position'))
    {
      $defaultnewsletter_position = $this->config->get('newsletter_position');
    }
    else
    {
      $defaultnewsletter_position = '1';
    }  
  
   $form['newsletter_position']= [
  '#type' => 'radios',
  '#title' => t('Newsletter Bar Position '),
  '#options' => array(1 => 'After Content Post', 2 => 'Before Content Post '),
  '#required'=>TRUE,
  '#default_value' =>$defaultnewsletter_position,
];
    
    
    /****************advance settings form **********************/
    
       $form['div_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"DIV element ID to target comments" (The comment box will appear after this DIV element)'),
      '#default_value' => $this->config->get('div_id'),
    ];
    
          $form['div_id_powerbar'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"DIV element ID to target powerbar" (The powerbar box will appear after this DIV element)'),
      '#default_value' => $this->config->get('div_id_powerbar'),
    ];
    
    
          $form['div_id_emotes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"DIV element ID to target emotes" (The emote box will appear after this DIV element)'),
      '#default_value' => $this->config->get('div_id_emotes'),
    ];
    
    
      if($this->config->get('priority'))
      {
      $defaultpriority = $this->config->get('priority');
      }
      else
      {
      $defaultpriority= '300';
      }  
          
      $form['priority'] = [
      '#type' => 'number',
      '#title' => $this->t('"Priority'),
      '#min' => 1,
      '#default_value' => $defaultpriority,
    ];
    
    
      if($this->config->get('newsletter_priority'))
      {
        $defaultnewsletter_priority = $this->config->get('newsletter_priority');
      }
      else
      {
        $defaultnewsletter_priority = '1';
      }  
      
    
       $form['newsletter_priority'] = [
      '#type' => 'number',
      '#title' => $this->t('Newsletter Priority	'),
      '#min' => 1,
      '#default_value' => $defaultnewsletter_priority,
      
    ];
    
    if($this->config->get('sorting'))
    {
    $defaultsorting = $this->config->get('sorting');
    }
    else
    {
    $defaultsorting = 'Latest';
    }  
      
    
   $form['sorting']= [
  '#type' => 'radios',
  '#title' => t('Sorting'),
  '#options' => array('Latest' => 'Latest', 'Oldest' => 'Oldest'),
  '#required'=>TRUE,
  '#default_value' =>$defaultsorting,
];


  if($this->config->get('recommendationsWideImages'))
  {
   $defaultrecommendationsWideImages = $this->config->get('recommendationsWideImages');
  }
  else
  {
   $defaultrecommendationsWideImages= 'false';
  }  
    

   $form['recommendationsWideImages']= [
  '#type' => 'radios',
  '#title' => t('Recommendations wide images'),
  '#options' => array('false' => 'No', 'true' => 'Yes'),
  '#default_value' =>$defaultrecommendationsWideImages,
];


  if($this->config->get('globalRecommendations'))
  {
   $defaultglobalRecommendations = $this->config->get('globalRecommendations');
  }
  else
  {
   $defaultglobalRecommendations = 'false';
  }  

   $form['globalRecommendations']= [
  '#type' => 'radios',
  '#title' => t('Global recommendations'),
  '#options' => array('false' => 'No', 'true' => 'Yes'),
  '#default_value' =>$defaultglobalRecommendations,
];

  if($this->config->get('darkMode'))
  {
   $defaultdarkMode = $this->config->get('darkMode');
  }
  else
  {
   $defaultdarkMode = 'false';
  }  
  
  $form['darkMode']= [
  '#type' => 'radios',
  '#title' => t('Dark mode'),
  '#options' => array('false' => 'No', 'true' => 'Yes'),
  '#default_value' =>$defaultdarkMode,
];

  if($this->config->get('hideRecommendedArticles'))
  {
   $defaulthideRecommendedArticles = $this->config->get('hideRecommendedArticles');
  }
  else
  {
   $defaulthideRecommendedArticles = 'false';
  } 

   $form['hideRecommendedArticles']= [
  '#type' => 'radios',
  '#title' => t('Hide recommended articles (Talk of the Town)'),
  '#options' => array('false' => 'No', 'true' => 'Yes'),
  '#default_value' =>$defaulthideRecommendedArticles,
];


  if($this->config->get('hideCommentInputBox'))
  {
   $defaulthideCommentInputBox = $this->config->get('hideCommentInputBox');
  }
  else
  {
   $defaulthideCommentInputBox = 'false';
  } 

   $form['hideCommentInputBox']= [
  '#type' => 'radios',
  '#title' => t(' Hide comment input box'),
  '#options' => array('false' => 'No', 'true' => 'Yes'),
  '#default_value' =>$defaulthideCommentInputBox,
];


  if($this->config->get('commentingClosed'))
  {
   $defaultcommentingClosed = $this->config->get('commentingClosed');
  }
  else
  {
   $defaultcommentingClosed= 'false';
  } 
  
  
   $form['commentingClosed']= [
  '#type' => 'radios',
  '#title' => t(' Commenting closed'),
  '#options' => array('false' => 'No', 'true' => 'Yes'),
  '#default_value' =>$defaultcommentingClosed,
];


  if($this->config->get('countToLoad'))
  {
   $defaultcountToLoad = $this->config->get('countToLoad');
  }
  else
  {
   $defaultcountToLoad = '5';
  } 
  

       $form['countToLoad'] = [
      '#type' => 'number',
      '#title' => $this->t('Count to load'),
      '#min' => 1,
      '#default_value' => $defaultcountToLoad,
    ];
    
 
  if($this->config->get('toxicityLimit'))
  {
   $defaulttoxicityLimit = $this->config->get('toxicityLimit');
  }
  else
  {
   $defaulttoxicityLimit = '80';
  } 
  
  
  

              $form['toxicityLimit'] = [
      '#type' => 'number',
      '#title' => $this->t('Toxicity Limit'),
      '#min' => 1,
      '#default_value' => $defaulttoxicityLimit,
    ];


      if($this->config->get('enabledComments'))
  {
   $defaultenabledComments = $this->config->get('enabledComments');
  }
  else
  {
   $defaultenabledComments = 'true';
  } 
  

       $form['enabledComments']= [
  '#type' => 'radios',
  '#title' => t('Enable comments'),
  '#options' => array('false' => 'No', 'true' => 'Yes'),
  '#default_value' =>$defaultenabledComments,
];

  if($this->config->get('size_emote'))
  {
   $defaultsize_emote = $this->config->get('size_emote');
  }
  else
  {
   $defaultsize_emote = '70';
  } 
  

    $form['size_emote'] = [
      '#type' => 'number',
      '#title' => $this->t('Emote size'),
      '#min' => 1,
      '#default_value' => $defaultsize_emote,
    ];
    
    
           $form['post_exceptions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"Remove vuukle from the posts (ids, separated by comma)'),
      '#default_value' => $this->config->get('post_exceptions'),
    ];
    
          $form['post_type_exceptions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"Remove vuukle from the post types (slugs, separated by comma)'),
      '#default_value' => $this->config->get('post_type_exceptions'),
    ];
    
    
          $form['category_exceptions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"Remove vuukle from the categories (slugs, separated by comma)'),
      '#default_value' => $this->config->get('category_exceptions'),
    ];

    
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {   
   
    $this->config->set('vuukle_api_key', $form_state->getValue('vuukle_api_key'))->save();
   
    $this->config->set('vuukle_content_type', $form_state->getValue('vuukle_content_type'))->save();
    
    $this->config->set('sso', $form_state->getValue('sso'))->save();
    
    $this->config->set('language', $form_state->getValue('language'))->save();
    
    $this->config->set('languageEnabledByDefault', $form_state->getValue('languageEnabledByDefault'))->save();
    
    $this->config->set('maxChars', $form_state->getValue('maxChars'))->save();
    
    $this->config->set('Param4', $form_state->getValue('Param4'))->save();
    
    $this->config->set('maxChars', $form_state->getValue('maxChars'))->save();
    
    $this->config->set('Param4', $form_state->getValue('Param4'))->save();
    
    $this->config->set('emote', $form_state->getValue('emote'))->save();
    
    $this->config->set('happy_text', $form_state->getValue('happy_text'))->save();
            
    $this->config->set('happy_url', $form_state->getValue('happy_url'))->save();
            
    $this->config->set('indifferent_text', $form_state->getValue('indifferent_text'))->save();
            
    $this->config->set('indifferent_url', $form_state->getValue('indifferent_url'))->save();
        
    $this->config->set('amused_text', $form_state->getValue('amused_text'))->save();
            
    $this->config->set('amused_url', $form_state->getValue('amused_url'))->save();
            
    $this->config->set('excited_text', $form_state->getValue('excited_text'))->save();
            
    $this->config->set('excited_url', $form_state->getValue('excited_url'))->save();
        
    $this->config->set('angry_text', $form_state->getValue('angry_text'))->save();
            
    $this->config->set('angry_url', $form_state->getValue('angry_url'))->save();
            
    $this->config->set('sad_text', $form_state->getValue('sad_text'))->save();
            
    $this->config->set('sad_url', $form_state->getValue('sad_url'))->save();
         
    $this->config->set('share', $form_state->getValue('share'))->save();
           
    $this->config->set('share_position', $form_state->getValue('share_position'))->save();
    
    $this->config->set('share_type', $form_state->getValue('share_type'))->save();
    
    $this->config->set('share_vertical_styles', $form_state->getValue('share_vertical_styles'))->save();

    $this->config->set('newsletter', $form_state->getValue('newsletter'))->save();
           
    $this->config->set('newsletter_position', $form_state->getValue('newsletter_position'))->save();
    
    $this->config->set('div_id', $form_state->getValue('div_id'))->save();
           
    $this->config->set('div_id_powerbar', $form_state->getValue('div_id_powerbar'))->save();
        
    $this->config->set('div_id_emotes', $form_state->getValue('div_id_emotes'))->save();
           
    $this->config->set('priority', $form_state->getValue('priority'))->save();

    $this->config->set('newsletter_priority', $form_state->getValue('newsletter_priority'))->save();
    
    $this->config->set('sorting', $form_state->getValue('sorting'))->save();
    
    $this->config->set('recommendationsWideImages', $form_state->getValue('recommendationsWideImages'))->save();
    
    $this->config->set('globalRecommendations', $form_state->getValue('globalRecommendations'))->save();
    
    $this->config->set('darkMode', $form_state->getValue('darkMode'))->save();
    
    $this->config->set('hideRecommendedArticles', $form_state->getValue('hideRecommendedArticles'))->save();
    
    $this->config->set('hideCommentInputBox', $form_state->getValue('hideCommentInputBox'))->save();
    
    $this->config->set('commentingClosed', $form_state->getValue('commentingClosed'))->save();
    
    $this->config->set('countToLoad', $form_state->getValue('countToLoad'))->save();
    
    $this->config->set('toxicityLimit', $form_state->getValue('toxicityLimit'))->save();

    $this->config->set('enabledComments', $form_state->getValue('enabledComments'))->save();
    
    $this->config->set('size_emote', $form_state->getValue('size_emote'))->save();

    $this->config->set('post_exceptions', $form_state->getValue('post_exceptions'))->save();
   
    $this->config->set('post_type_exceptions', $form_state->getValue('post_type_exceptions'))->save();
    
    $this->config->set('category_exceptions', $form_state->getValue('category_exceptions'))->save();
    
    parent::submitForm($form, $form_state);
  }

}
